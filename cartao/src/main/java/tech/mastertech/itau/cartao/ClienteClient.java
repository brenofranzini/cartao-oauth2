package tech.mastertech.itau.cartao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente")
public interface ClienteClient {

	@GetMapping("cliente/{id}")
	Cliente buscar(@PathVariable int id);
}
