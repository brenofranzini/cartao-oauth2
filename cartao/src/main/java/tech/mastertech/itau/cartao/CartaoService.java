package tech.mastertech.itau.cartao;

import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartaoService {
	@Autowired
	private CartaoRepository cartaoRepository;
	private Logger log;

	@Autowired
	private ClienteClient clienteService;

	public Cartao criar(Cartao cartao) {

		try {
			Cliente cliente = clienteService.buscar(cartao.getIdCliente());

			if (cliente != null) {
				cartao.setNumero(gerarNumero());
				cartao.setAtivo(false);

				return cartaoRepository.save(cartao);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

	public Optional<Cartao> buscar(String numero) {
		return cartaoRepository.findById(numero);
	}

	private String gerarNumero() {
		Random random = new Random();
		String numero = "";

		for (int i = 0; i < 16; i++) {
			numero += random.nextInt(9);
		}

		if (cartaoRepository.findById(numero).isPresent()) {
			return gerarNumero();
		}

		return numero;
	}

	public void ativar(String numero) {
		Optional<Cartao> cartaoOptional = cartaoRepository.findById(numero);

		if (!cartaoOptional.isPresent()) {
			throw new ValidacaoException("numero", "Cartão não encontrado.");
		}

		Cartao cartao = cartaoOptional.get();

		if (cartao.getAtivo()) {
			throw new ValidacaoException("numero", "Cartão já está ativo.");
		}

		cartao.setAtivo(true);
		cartaoRepository.save(cartao);
	}
}
