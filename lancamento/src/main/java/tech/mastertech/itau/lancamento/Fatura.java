package tech.mastertech.itau.lancamento;

import java.math.BigDecimal;

public class Fatura {

	private BigDecimal total;

	private Iterable<Lancamento> lancamentos;

	public Iterable<Lancamento> getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(Iterable<Lancamento> lancamentos) {
		this.lancamentos = lancamentos;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
