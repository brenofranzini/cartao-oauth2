package tech.mastertech.itau.cliente;

import java.util.Map;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

public class CustomPrincipalExtractor implements PrincipalExtractor {

	@Override
	public Object extractPrincipal(Map<String, Object> map) {
		Usuario usuario = new Usuario();
		usuario.setCpf((String) map.get("cpf"));

		return usuario;
	}

}
