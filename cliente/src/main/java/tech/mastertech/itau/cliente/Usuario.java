package tech.mastertech.itau.cliente;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.br.CPF;

public class Usuario {
	@NotBlank
	@CPF
	private String cpf;

	@NotBlank
	private String senha;

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
