package tech.mastertech.itau.cliente;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Component
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private AuthClient auth;

	@Transactional(rollbackOn = Exception.class)
	public Cliente criar(ClienteDTO clienteDTO) {
		if (clienteDTO.getId() != 0) {
			throw new ValidacaoException("id", "Id não pode ser definido ao criar um cliente");
		}
		Usuario usuario = new Usuario();
		
		usuario.setCpf(clienteDTO.getCpf());
		usuario.setSenha(clienteDTO.getSenha());
		
		auth.criar(usuario);

		Cliente cliente = new Cliente();
		
		cliente.setCpf(clienteDTO.getCpf());
		cliente.setNome(clienteDTO.getNome());

		return clienteRepository.save(cliente);
	}

	public Optional<Cliente> buscar(int id) {
		return clienteRepository.findById(id);
	}
}
