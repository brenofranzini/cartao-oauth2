package tech.mastertech.itau.cliente;


import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
  Optional<Cliente> findByCpf(String cpf);
}
