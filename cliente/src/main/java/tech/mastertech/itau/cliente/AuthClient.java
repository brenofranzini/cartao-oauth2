package tech.mastertech.itau.cliente;

import java.security.Principal;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "authserver")
public interface AuthClient {

	@GetMapping("/me")
	public Map<String, String> validar(Principal principal);

	@PostMapping("/usuario")
	public Usuario criar(@RequestBody Usuario usuario);
}
