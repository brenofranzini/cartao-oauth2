package com.poc.authserver.usuario;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/me")
	public Map<String, String> validar(Principal principal) {
		Map<String, String> map = new HashMap<>();
		map.put("cpf", principal.getName());

		return map;
	}
	
	@PostMapping("/usuario")
	public Usuario criar(@RequestBody Usuario usuario) {
		return usuarioService.criar(usuario);
	}
}