package com.poc.authserver.usuario;

import java.util.Collections;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UsuarioService implements UserDetailsService {
	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private UsuarioRepository repository;

	@PostConstruct
	public void popular() {
		Usuario usuario = new Usuario();
		
		usuario.setCpf("056.197.450-07");
		usuario.setSenha(encoder.encode("admin123"));

		repository.save(usuario);
	}

//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		Optional<Usuario> optional = repository.findByNome(username);
//
//		if (!optional.isPresent()) {
//			throw new UsernameNotFoundException("Usuário não encontrado");
//		}
//
//		Usuario usuario = optional.get();
//
//		SimpleGrantedAuthority authority = new SimpleGrantedAuthority("user");
//
//		return new User(usuario.getNome(), usuario.getSenha(), Collections.singletonList(authority));
//	}

	public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
		Optional<Usuario> optional = repository.findByCpf(cpf);

		if (!optional.isPresent()) {
			throw new UsernameNotFoundException("Usuário não encontrado");
		}

		Usuario usuario = optional.get();

		SimpleGrantedAuthority authority = new SimpleGrantedAuthority("user");

		return new User(usuario.getCpf(), usuario.getSenha(), Collections.singletonList(authority));
	}

	public Usuario criar(Usuario usuario) {
		Optional<Usuario> usuarioOptional = repository.findByCpf(usuario.getCpf());
		
		if(!usuarioOptional.isPresent()) {
			String hash = encoder.encode(usuario.getSenha());
			usuario.setSenha(hash);
			
			return repository.save(usuario);
		}
		throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
	}
}
