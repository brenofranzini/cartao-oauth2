package com.poc.authserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;

import com.poc.authserver.usuario.UsuarioService;

@Configuration
@EnableAuthorizationServer
public class AuthConfig extends AuthorizationServerConfigurerAdapter{

  @Autowired
  private BCryptPasswordEncoder encoder;
  
  @Autowired
  private AuthenticationManager manager;
  
  @Autowired
  private UsuarioService usuarioService;
  
  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    clients.inMemory()
     .withClient("cliente")
     .secret(encoder.encode("cliente123"))
     .authorizedGrantTypes("check_token", "password", "refresh_token")
     .scopes("all")
     .and()
     .withClient("cartao")
     .secret(encoder.encode("cartao123"))
     .authorizedGrantTypes("check_token", "password", "refresh_token")
     .scopes("all")
     .and()
     .withClient("lancamento")
     .secret(encoder.encode("lancamento123"))
     .authorizedGrantTypes("check_token", "password", "refresh_token")
     .scopes("all");
  }
  
  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    endpoints.authenticationManager(manager).userDetailsService(usuarioService);
  }
}
